# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

try:
    long_description = open("README.rst").read()
except IOError:
    long_description = ""

setup(
    name="terminator-config-generator",
    version="0.1.0",
    description="Generate configuration file for terminator (https://launchpad.net/terminator)",
    license="MIT",
    author="David Levanon",
    packages=find_packages(),
    install_requires=[],
    long_description=long_description,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
    ]
)
