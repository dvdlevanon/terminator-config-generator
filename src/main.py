import sys
import argparse
import os.path 

def exit(code, msg):
	sys.stderr.write("%s\n" % msg)
	sys.exit(code)

def conf_dir_arg(dirStr):
	return os.path.normpath(os.path.join(os.getcwd(), dirStr))
	
def output_file_arg(fileStr):
	return os.path.normpath(os.path.join(os.getcwd(), fileStr))
	
def build_command_line_parser():
	parser = argparse.ArgumentParser(description='Generate terminator configuration file')

	parser.add_argument('-c', '--conf-dir', 
		metavar='DIR',
		type=conf_dir_arg,
		default='conf',
		help='A directory containing the configuration directory')
	
	parser.add_argument('-o', '--output-file', 
		metavar='FILE',
		type=output_file_arg,
		default='conf',
		help='The path to the generated terminator-config file')
	
	return parser

def main(rawArgs):
	parser = build_command_line_parser()
	args = parser.parse_args(args=rawArgs)
	
	conf_dir=args.conf_dir
	output_file=args.output_file
	
	if not os.path.isdir(conf_dir):
		exit(1, 'Error: Invalid conf dir %s' % conf_dir)
	
	sys.stdout.write('done! %s\n' % args.conf_dir)

if __name__ == '__main__':
	main(sys.argv[1:])
